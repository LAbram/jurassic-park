<?php
require "vendor/autoload.php";
use Michelf\Markdown;

Flight::route('/', function(){
    $data = [
        'dinos' =>  getDino()
    ];

    Flight::view()->display('dino.twig', $data);
});

Flight::route('/dinosaur/@slug', function($slug){

   

    $data = [
        'dino' =>  getDinospec($slug),
        'randino' => getRandomDino(),

        'desc' => readFileContent('pages\aled.md')
    ];
  

    Flight::view()->display('dinospec.twig', $data);
});



$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
});

Flight::start();
?>